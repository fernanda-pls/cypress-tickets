describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))

    it("fills all the text input fields", () => {
        const firstName = "Fulana";
        const lastName = "Silva";
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("fulana@fulanamail.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'VIP' ticket type", () => {
        cy.get("#vip").check();
    });

    it("selects 'Social Media' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("selects Friend' and 'Publication', then uncheck 'Friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    // assertions
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        cy.get("#email")
        .as("email")
        .type("fulana-fulanamail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("fulana@gmail.com");

        cy.get("#email.invalid").should("not.exist");
    });

    // Teste e2e 

    it("fills and reset the form", () => {
        const firstName = "Fulana";
        const lastName = "Silva";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("fulana@fulanamail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("Vegetarian");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    // Comandos customizados

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@example.com"
        }

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled")

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    })

})